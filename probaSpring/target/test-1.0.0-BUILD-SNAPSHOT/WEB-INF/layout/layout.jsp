<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" /></title>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="<c:url value="/resources/css/default.css"/>" rel="stylesheet" type="text/css" />
  
</head>
 <body class="layout">
        <div class="backgroundContainer">
            <div class="contener">
                <div class="searchSmall">
                    <tiles:insertAttribute name="header" />
                    <div class="searchSmallForm"></div>
                </div>
                <tiles:insertAttribute name="body" />
            </div>
        </div>
        <tiles:insertAttribute name="footer" />
    </body>
</html>