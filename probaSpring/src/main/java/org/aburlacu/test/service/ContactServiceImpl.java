package org.aburlacu.test.service;

import java.util.List;

import org.aburlacu.test.dao.ContactDAO;
import org.aburlacu.test.model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * ContactServiceImpl Class
 * @author Alexandru Burlacu
 */
@Service
public class ContactServiceImpl implements ContactService {
 
    @Autowired
    private ContactDAO contactDAO;

    /**
     * Add contact entry
     * 
     * @param Contact contact - contact object
     * @author Alexandru Burlacu
     */
    @Transactional
    public void addContact(Contact contact) {
        contactDAO.addContact(contact);
    }

    /**
     * Get list of contacts
     * 
     * @return List<Contact> - list of contacts
     * @author Alexandru Burlacu
     */
    @Transactional
    public List<Contact> listContact() {
 
        return contactDAO.listContact();
    }
    /**
     * Remove contact entry by id
     * 
     * @param Integer id - contact id
     * @author Alexandru Burlacu
     */
    @Transactional
    public void removeContact(Integer id) {
        contactDAO.removeContact(id);
    }

    /**
     * Get contact by id
     * 
     * @param Integer id - contact id
     * @return Contact - contact object
     * @author Alexandru Burlacu
     */
    @Transactional
    public Contact getContactById(Integer contactId) {
       return contactDAO.getContactById(contactId);
  }
    
    /**
     * Get contacts by name
     * 
     * @param String name - name
     * @return List<Contact> - list of contacts
     * @author Alexandru Burlacu
     */
    @Transactional
    public List<Contact> getContactsByName(String name) {
        return contactDAO.getContactsByName(name);
    }
}
