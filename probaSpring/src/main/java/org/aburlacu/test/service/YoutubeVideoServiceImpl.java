package org.aburlacu.test.service;

import java.text.SimpleDateFormat;
import java.util.List;

import org.aburlacu.test.dao.YoutubeVideoDAO;
import org.aburlacu.test.model.YoutubeVideo;
import org.aburlacu.test.util.CustomTimestampEditor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.java6.auth.oauth2.FileCredentialStore;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoSnippet;
import com.google.api.services.youtube.model.VideoStatus;
import com.google.common.base.CharMatcher;
import com.google.common.collect.Lists;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * YoutubeVideoServiceImpl Class
 * 
 * @author Alexandru Burlacu
 */
@Service
public class YoutubeVideoServiceImpl implements YoutubeVideoService {

    @Autowired
    private YoutubeVideoDAO youtubeVideoDAO;

    /** Global instance of the HTTP transport. */
    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();

    /** Global instance of Youtube object to make all API requests. */
    private static YouTube youtube;

    /*
     * Global instance of the format used for the video being uploaded (MIME
     * type).
     */
    private static String VIDEO_FILE_FORMAT = "video/*";
    
    private static Logger logger = Logger.getLogger(YoutubeVideoServiceImpl.class);
    
    
    @Value("${uploads.folder}")
    private String uploadFolder;

    /**
     * Add video entry
     * 
     * @param YoutubeVideo video - video object
     * @author Alexandru Burlacu
     */
    @Transactional
    public boolean addVideo(YoutubeVideo youtubeVideo) {
        boolean result =  false;
        
        Date date = new Date() ;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String prefix = dateFormat.format(date).toString();
        String orgName = youtubeVideo.getFileName();
        String fileName = prefix + orgName;
        fileName.replaceAll("\\s","");
        String filePath = uploadFolder + fileName;
        File dest = new File(filePath);
        try {
            youtubeVideo.getFileData().transferTo(dest);
            result = youtubeVideoDAO.addVideo(youtubeVideo);
        } catch (IllegalStateException e) {
            e.printStackTrace();
            logger.error("File uploaded failed:" + orgName);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("File uploaded failed:" + orgName);
        }
        youtubeVideo.setFilePath(filePath);
        result = youtubeVideoDAO.addVideo(youtubeVideo);
        return result;
    }

    /**
     * Get list of tasks
     * 
     * @return List<YoutubeVideo> - list of tasks
     * @author Alexandru Burlacu
     */
    @Transactional
    public List<YoutubeVideo> listVideo() {
        return youtubeVideoDAO.listVideo();
    }

    /**
     * Remove video entry by id
     * 
     * @param Integer
     *            id - task id
     * @author Alexandru Burlacu
     */
    @Transactional
    public void removeVideo(Integer id) {
        youtubeVideoDAO.removeVideo(id);
    }

    /**
     * Update video entry
     * 
     * @param YoutubeVideo
     *            video - video object
     * @author Alexandru Burlacu
     */
    @Transactional
    public void updateVideo(YoutubeVideo youtubeVideo) {
        youtubeVideoDAO.updateVideo(youtubeVideo);
    }

    /**
     * Get video by id
     * 
     * @param Integer
     *            videoId - task id
     * @return YoutubeVideo - video object
     * @author Alexandru Burlacu
     */
    @Transactional
    public YoutubeVideo getVideoById(Integer videoId) {
        return youtubeVideoDAO.getVideoById(videoId);
    }

    /**
     * Authorizes the installed application to access user's protected data.
     * 
     * @param scopes
     *            list of scopes needed to run youtube upload.
     */
    private static Credential authorize(List<String> scopes) throws Exception {

        // Load client secrets.
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(
                JSON_FACTORY,
                YoutubeVideoServiceImpl.class.getResourceAsStream("/client_secrets.json"));

        // Checks that the defaults have been replaced (Default =
        // "Enter X here").
        if (clientSecrets.getDetails().getClientId().startsWith("Enter")
                || clientSecrets.getDetails().getClientSecret()
                        .startsWith("Enter ")) {
            logger.error("Enter Client ID and Secret from https://code.google.com/apis/console/?api=youtube"
                    + "into youtube-cmdline-uploadvideo-sample/src/main/resources/client_secrets.json");
                    
            System.exit(1);
        }

        // Set up file credential store.
        FileCredentialStore credentialStore = new FileCredentialStore(new File(
                System.getProperty("user.home"),
                ".credentials/youtube-api-uploadvideo.json"), JSON_FACTORY);

        // Set up authorization code flow.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, scopes)
                .setCredentialStore(credentialStore).build();

        // Build the local server and bind it to port 9000
        LocalServerReceiver localReceiver = new LocalServerReceiver.Builder()
                .setPort(8080).build();

        // Authorize.
        return new AuthorizationCodeInstalledApp(flow, localReceiver)
                .authorize("user");
    }

    /**
     * Uploads user selected video in the project folder to the user's YouTube
     * account using OAuth2 for authentication.
     * 
     * @param args
     *            command line args (not used).
     */
    public static void upload() {

        // Scope required to upload to YouTube.
        List<String> scopes = Lists
                .newArrayList("https://www.googleapis.com/auth/youtube.upload");

        try {
            // Authorization.
            Credential credential = authorize(scopes);

            // YouTube object used to make all API requests.
            youtube = new YouTube.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                    credential).setApplicationName(
                    "youtube-cmdline-uploadvideo-sample").build();

            // We get the user selected local video file to upload.
            File videoFile = getVideoFromUser();
            logger.info("You chose " + videoFile + " to upload.");

            // Add extra information to the video before uploading.
            Video videoObjectDefiningMetadata = new Video();

            /*
             * Set the video to public, so it is available to everyone (what
             * most people want). This is actually the default, but I wanted you
             * to see what it looked like in case you need to set it to
             * "unlisted" or "private" via API.
             */
            VideoStatus status = new VideoStatus();
            status.setPrivacyStatus("public");
            videoObjectDefiningMetadata.setStatus(status);

            // We set a majority of the metadata with the VideoSnippet object.
            VideoSnippet snippet = new VideoSnippet();

            /*
             * The Calendar instance is used to create a unique name and
             * description for test purposes, so you can see multiple files
             * being uploaded. You will want to remove this from your project
             * and use your own standard names.
             */
            Calendar cal = Calendar.getInstance();
            snippet.setTitle("Test Upload via Java on " + cal.getTime());
            snippet.setDescription("Video uploaded via YouTube Data API V3 using the Java library "
                    + "on " + cal.getTime());

            // Set your keywords.
            List<String> tags = new ArrayList<String>();
            tags.add("test");
            tags.add("example");
            tags.add("java");
            tags.add("YouTube Data API V3");
            tags.add("erase me");
            snippet.setTags(tags);

            // Set completed snippet to the video object.
            videoObjectDefiningMetadata.setSnippet(snippet);
            
            InputStreamContent mediaContent = new InputStreamContent(
                    VIDEO_FILE_FORMAT, new BufferedInputStream(
                            new FileInputStream(videoFile)));
            mediaContent.setLength(videoFile.length());

            /*
             * The upload command includes: 1. Information we want returned
             * after file is successfully uploaded. 2. Metadata we want
             * associated with the uploaded video. 3. Video file itself.
             */
            YouTube.Videos.Insert videoInsert = youtube.videos().insert(
                    "snippet,statistics,status", videoObjectDefiningMetadata,
                    mediaContent);

            // Set the upload type and add event listener.
            MediaHttpUploader uploader = videoInsert.getMediaHttpUploader();

            /*
             * Sets whether direct media upload is enabled or disabled. True =
             * whole media content is uploaded in a single request. False
             * (default) = resumable media upload protocol to upload in data
             * chunks.
             */
            uploader.setDirectUploadEnabled(false);

            MediaHttpUploaderProgressListener progressListener = new MediaHttpUploaderProgressListener() {
                public void progressChanged(MediaHttpUploader uploader)
                        throws IOException {
                    switch (uploader.getUploadState()) {
                    case INITIATION_STARTED:
                        logger.info("Initiation Started");
                        break;
                    case INITIATION_COMPLETE:
                        logger.info("Initiation Completed");
                        break;
                    case MEDIA_IN_PROGRESS:
                        logger.info("Upload in progress");
                        logger.info("Upload percentage: "
                                + uploader.getProgress());
                        break;
                    case MEDIA_COMPLETE:
                        logger.info("Upload Completed!");
                        break;
                    case NOT_STARTED:
                        logger.info("Upload Not Started!");
                        break;
                    }
                }
            };
            uploader.setProgressListener(progressListener);

            // Execute upload.
            Video returnedVideo = videoInsert.execute();

            // Print out returned results.
            logger.info("\n================== Returned Video ==================\n");
            logger.info("  - Id: " + returnedVideo.getId());
            logger.info("  - Title: "
                    + returnedVideo.getSnippet().getTitle());
            logger.info("  - Tags: "
                    + returnedVideo.getSnippet().getTags());
            logger.info("  - Privacy Status: "
                    + returnedVideo.getStatus().getPrivacyStatus());
            logger.info("  - Video Count: "
                    + returnedVideo.getStatistics().getViewCount());

        } catch (GoogleJsonResponseException e) {
            logger.error("GoogleJsonResponseException code: "
                    + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("IOException: " + e.getMessage());
            e.printStackTrace();
        } catch (Throwable t) {
            logger.error("Throwable: " + t.getMessage());
            t.printStackTrace();
        }
    }

    /**
     * Gets the user selected local video file to upload.
     */
    private static File getVideoFromUser() throws IOException {
        File[] listOfVideoFiles = getLocalVideoFiles();
        return getUserChoice(listOfVideoFiles);
    }

    /**
     * Gets an array of videos in the current directory.
     */
    private static File[] getLocalVideoFiles() throws IOException {

        File currentDirectory = new File(".");
        logger.info("Video files from "
                + currentDirectory.getAbsolutePath() + ":");

        // Filters out video files. This list of video extensions is not
        // comprehensive.
        FilenameFilter videoFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                String lowercaseName = name.toLowerCase();
                if (lowercaseName.endsWith(".webm")
                        || lowercaseName.endsWith(".flv")
                        || lowercaseName.endsWith(".f4v")
                        || lowercaseName.endsWith(".mov")
                        || lowercaseName.endsWith(".mpg")) {
                    return true;
                } else {
                    return false;
                }
            }
        };
        return currentDirectory.listFiles(videoFilter);
    }

    /**
     * Outputs video file options to the user, records user selection, and
     * returns the video (File object).
     * 
     * @param videoFiles
     *            Array of video File objects
     */
    private static File getUserChoice(File videoFiles[]) throws IOException {

        if (videoFiles.length < 1) {
            throw new IllegalArgumentException(
                    "No video files in this directory.");
        }

        for (int i = 0; i < videoFiles.length; i++) {
            logger.info(" " + i + " = " + videoFiles[i].getName());
        }

        BufferedReader bReader = new BufferedReader(new InputStreamReader(
                System.in));
        //by default only first f=video is selected from the list
        String inputChoice = "0";

        /*do {
            System.out
                    .print("Choose the number of the video file you want to upload: ");
            inputChoice = bReader.readLine();
        } while (!isValidIntegerSelection(inputChoice, videoFiles.length));*/
        return videoFiles[Integer.parseInt(inputChoice)];
    }

    /**
     * Checks if string contains a valid, positive integer that is less than
     * max. Please note, I am not testing the upper limit of an integer
     * (2,147,483,647). I just go up to 999,999,999.
     * 
     * @param input
     *            String to test.
     * @param max
     *            Integer must be less then this Maximum number.
     */
    public static boolean isValidIntegerSelection(String input, int max) {
        if (input.length() > 9)
            return false;

        boolean validNumber = false;
        // Only accepts positive numbers of up to 9 numbers.
        Pattern intsOnly = Pattern.compile("^\\d{1,9}$");
        Matcher makeMatch = intsOnly.matcher(input);

        if (makeMatch.find()) {
            int number = Integer.parseInt(makeMatch.group());
            if ((number >= 0) && (number < max)) {
                validNumber = true;
            }
        }
        return validNumber;
    }
}
