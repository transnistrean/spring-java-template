package org.aburlacu.test.service;

import java.util.List;

import org.aburlacu.test.model.Contact;
import org.springframework.stereotype.Component;

/**
 * Contact Service Interface
 * @author Alexandru Burlacu
 */

public interface ContactService {

    /**
     * Add contact entry
     * 
     * @param Contact contact - contact object
     * @author Alexandru Burlacu
     */
    public void addContact(Contact contact);
    
    /**
     * Get list of contacts
     * 
     * @return List<Contact> - list of contacts
     * @author Alexandru Burlacu
     */
    public List<Contact> listContact();
    
    /**
     * Remove contact entry by id
     * 
     * @param Integer id - contact id
     * @author Alexandru Burlacu
     */
    public void removeContact(Integer id);
    
    /**
     * Get contact by id
     * 
     * @param Integer id - contact id
     * @return Contact - contact object
     * @author Alexandru Burlacu
     */
    public Contact getContactById(Integer id);
    
    /**
     * Get contacts by name
     * 
     * @param String name - name
     * @return List<Contact> - list of contacts
     * @author Alexandru Burlacu
     */
    
    public List<Contact> getContactsByName(String name);
}
