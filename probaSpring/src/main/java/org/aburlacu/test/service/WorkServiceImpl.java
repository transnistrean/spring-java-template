package org.aburlacu.test.service;

import java.util.List;

import org.aburlacu.test.dao.WorkDAO;
import org.aburlacu.test.model.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * WorkServiceImpl Class
 * @author Alexandru Burlacu
 */
@Service
public class WorkServiceImpl implements WorkService {
 
    @Autowired
    private WorkDAO workDAO;
  
  /**
   * Add work entry
   * 
   * @param Work work - work object
   * @author Alexandru Burlacu
   */
  @Transactional
    public void addWork(Work work) {
        workDAO.addWork(work);
    }
 
  /**
   * Get list of tasks
   * 
   * @return List<Work> - list of tasks
   * @author Alexandru Burlacu
   */
  @Transactional
    public List<Work> listWork() {
        return workDAO.listWork();
    }
  
  /**
   * Remove work entry by id
   * 
   * @param Integer id - task id
   * @author Alexandru Burlacu
   */
  @Transactional
    public void removeWork(Integer id) {
        workDAO.removeWork(id);
    }
  
  /**
   * Update work entry
   * 
   * @param Work work - work object
   * @author Alexandru Burlacu
   */
  @Transactional
  public void updateWork(Work work) {
      workDAO.updateWork(work);
  }
  
  /**
   * Get work by id
   * 
   * @param Integer workId - task id
   * @return Work - work object
   * @author Alexandru Burlacu
   */
  @Transactional
  public Work getWorkById(Integer workId) {
     return workDAO.getWorkById(workId);
}
}
