package org.aburlacu.test.service;

import java.util.List;

import org.aburlacu.test.model.YoutubeVideo;

/**
 * YoutubeVideo Service Interface
 * @author Alexandru Burlacu
 */
public interface YoutubeVideoService {

    /**
     * Add video entry
     * 
     * @param YoutubeVideo video - video object
     * @author Alexandru Burlacu
     */
    public boolean addVideo(YoutubeVideo youtubeVideo);
    
    /**
     * Get list of tasks
     * 
     * @return List<YoutubeVideo> - list of tasks
     * @author Alexandru Burlacu
     */
    public List<YoutubeVideo> listVideo();
    
    /**
     * Remove video entry by id
     * 
     * @param Integer id - task id
     * @author Alexandru Burlacu
     */
    public void removeVideo(Integer id);
    
    /**
     * Update video entry
     * 
     * @param YoutubeVideo video - video object
     * @author Alexandru Burlacu
     */
    public void updateVideo(YoutubeVideo youtubeVideo);
    
    /**
     * Get video by id
     * 
     * @param Integer videoId - task id
     * @return YoutubeVideo - video object
     * @author Alexandru Burlacu
     */
    public YoutubeVideo getVideoById(Integer id);
}
