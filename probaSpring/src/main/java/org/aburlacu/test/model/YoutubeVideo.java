package org.aburlacu.test.model;


import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.StyledEditorKit.BoldAction;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.commons.CommonsMultipartFile;



/**
 * YoutubeVideo Model
 * @author Alexandru Burlacu
 */

@Entity
@Table(name="VIDEO")
public class YoutubeVideo {
    
    @Id
    @Column(name="ID")
    @GeneratedValue
    private Integer id;
    
    @NotEmpty
    @Size(min=2, max = 100)
    @Column(name="FILE_NAME")
    private String fileName;
    
    @Column(name="IS_REMOVED" )
    private Boolean isRemoved = false;
    
    @NotEmpty
    @Size(min=2, max = 100)
    @Column(name="VIDEO_NAME")
    private String videoName;
    
    @NotEmpty
    @Size(min=2, max = 100)
    @Column(name="FILE_PATH")
    private String filePath;

    @javax.persistence.Transient
    private CommonsMultipartFile fileData;

    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public CommonsMultipartFile getFileData()
    {
      return fileData;
    }
     
    public void setFileData(CommonsMultipartFile fileData)
    {
      this.fileData = fileData;
      if(fileName == null) {
          this.setFileName(fileData.getOriginalFilename());
      }
      if(filePath == null) {
          this.setFilePath(fileData.getOriginalFilename());
      }
    }
    
    public Boolean getIsRemoved() {
        return isRemoved;
    }

    public void setIsRemoved(Boolean isRemoved) {
        this.isRemoved = isRemoved;
    }
    
    public String getVideoName() {
        return videoName;
    }
    
    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }
    
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

}
