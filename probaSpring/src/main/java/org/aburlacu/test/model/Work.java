package org.aburlacu.test.model;


import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.swing.text.StyledEditorKit.BoldAction;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Work Model
 * @author Alexandru Burlacu
 */

@Entity
@Table(name="WORK")
public class Work {
    
	@Id
    @Column(name="ID")
    @GeneratedValue
    private Integer id;
    
    @NotEmpty
    @Size(min=2, max = 100)
    @Column(name="TASK_NAME")
    private String taskName;
    
    @Size(min=10, max = 255)
    @NotNull
    @Column(name="DESCRIPTION")
    private String description;
    
    @NotNull
    @Column(name="START_DATE")
    private Timestamp startDate;
    
    @NotNull
    @Column(name="END_DATE")
    private Timestamp endDate;
    
    @NotNull
    @Column(name="STATUS")
    private String status;
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL }, optional = true)
	@JoinColumn(name = "contact_id", nullable = false)
    private Contact contact;
    
    @Column(name="IS_REMOVED" )
    private Boolean isRemoved = false;
    
    public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getIsRemoved() {
		return isRemoved;
	}

	public void setIsRemoved(Boolean isRemoved) {
		this.isRemoved = isRemoved;
	}	
}
