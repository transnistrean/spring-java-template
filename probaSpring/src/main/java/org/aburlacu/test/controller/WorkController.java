package org.aburlacu.test.controller;




import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Map;

import org.aburlacu.test.model.Contact;
import org.aburlacu.test.model.Work;
import org.aburlacu.test.service.ContactService;
import org.aburlacu.test.service.WorkService;
import org.aburlacu.test.util.CustomTimestampEditor;
import org.jboss.logging.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


/**
 * Work Controller 
 * @author Alexandru Burlacu
 */
@Controller
public class WorkController {

    @Autowired
    private WorkService workService;

    @Autowired
    private Validator validator;
    
    @Autowired
    private ContactService contactService;
    
    @InitBinder
    public void binder(WebDataBinder binder) {
        //Create a new CustomDateEditor
        CustomTimestampEditor editor = new CustomTimestampEditor(("yyyy-mm-dd"));
        //Register it as custom editor for the Date type
        binder.registerCustomEditor(Timestamp.class, editor);
    }

    @RequestMapping(value = { "/work.html", "work.html" })
    public String listWorks(Map<String, Object> map) {

        map.put("work", new Work());
        map.put("workList", workService.listWork());
        
        //add  statuses
        ArrayList statusList = new ArrayList();
        statusList.add("In progress");
        statusList.add("Done");
        statusList.add("Canceled");
        map.put("statusList", statusList);
        map.put("contactList", contactService.listContact());

        return "work";
    }

    @RequestMapping(value = "/add-work.html", method = RequestMethod.POST)
    public String addWork(@ModelAttribute("work") Work work,
            BindingResult result, @Param Integer contactId, Map<String, Object> map) {
        String url = "work";
        validator.validate(work, result);
        if(!result.hasErrors()) {
            Contact contact = contactService.getContactById(contactId);
            work.setContact(contact);
            workService.addWork(work);
            url = "redirect:/work.html";
        } else {
             map.put("workList", workService.listWork());
             //add  statuses
             ArrayList statusList = new ArrayList();
             statusList.add("In progress");
             statusList.add("Done");
             statusList.add("Canceled");
             map.put("statusList", statusList);
             map.put("contactList", contactService.listContact());
        }

        
        return url;
    }

    @RequestMapping("/delete-work.html")
    public String deleteWork(@Param Integer workId) {
        workService.removeWork(workId);
        return "redirect:/work.html";
    }

    @RequestMapping(value = "/update-work.html", method = RequestMethod.POST)
    public String updateWork(@ModelAttribute("work") Work work,
            BindingResult result) {
        workService.updateWork(work);
        return "redirect:/work.html";
    }
    
    @RequestMapping(value = "/work-done.html")
    public String doneWork(@Param Integer workId) {
        Work work = workService.getWorkById(workId);
        work.setStatus("Done");
        workService.updateWork(work);
        return "redirect:/work.html";
    }
    @RequestMapping(value = "/cancel-work.html")
    public String cancelWork(@Param Integer workId) {
        Work work = workService.getWorkById(workId);
        work.setStatus("Canceled");
        workService.updateWork(work);
        return "redirect:/work.html";
    }
}
