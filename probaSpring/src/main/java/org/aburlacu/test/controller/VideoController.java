package org.aburlacu.test.controller;




import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Map;

import org.aburlacu.test.model.Contact;
import org.aburlacu.test.model.Work;
import org.aburlacu.test.model.YoutubeVideo;
import org.aburlacu.test.service.ContactService;
import org.aburlacu.test.service.WorkService;
import org.aburlacu.test.service.YoutubeVideoService;
import org.aburlacu.test.util.CustomTimestampEditor;
import org.jboss.logging.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;




/**
 * Work Controller 
 * @author Alexandru Burlacu
 */
@Controller
public class VideoController {

    @Autowired
    private ContactService contactService;
    
    @Autowired
    private WorkService workService;
    
    
    
    @Autowired
    private YoutubeVideoService youtubeVideoService;
    
    

    @Autowired
    private Validator validator;
    
    
    @InitBinder
    public void binder(WebDataBinder binder) {
        //Create a new CustomDateEditor
        CustomTimestampEditor editor = new CustomTimestampEditor(("yyyy-mm-dd"));
        //Register it as custom editor for the Date type
        binder.registerCustomEditor(Timestamp.class, editor);
    }

    @RequestMapping(value={"/index.html", "uploads.html"})
    public String listUploads(Map<String, Object> map) {

        map.put("video", new YoutubeVideo());
        map.put("videoList", youtubeVideoService.listVideo());
        
        //add  statuses
        ArrayList statusList = new ArrayList();
        statusList.add("In progress");
        statusList.add("Done");
        statusList.add("Canceled");
        map.put("statusList", statusList);
       
        return "uploads";
    }

    @RequestMapping(value = "/insert-video.html", method = RequestMethod.POST)
    public String addVideo(@ModelAttribute("video") YoutubeVideo youtubeVideo,
            BindingResult result,  Map<String, Object> map) {
        String url = "uploads";
        validator.validate(youtubeVideo, result);
         if(!result.hasErrors()) {
            
            youtubeVideoService.addVideo(youtubeVideo);
            url = "redirect:/uploads.html";
             //url = youtubeVideoService.addVideo(youtubeVideo) ? "redirect:/uploads.html" : url;
         } else {
            map.put("videoList", youtubeVideoService.listVideo());
         }
        
        
        return url;
    }

    @RequestMapping("/delete-video.html")
    public String deleteWork(@Param Integer videoId) {
        youtubeVideoService.removeVideo(videoId);
        return "redirect:/uploads.html";
    }

    @RequestMapping(value = "/upload-to-youtube.html")
    public String uploadToYoutube(@Param Integer workId) {
        
        return "redirect:/uploads.html";
    }
    
//    @RequestMapping(value = "/update-video.html", method = RequestMethod.POST)
//    public String updateWork(@ModelAttribute("work") Work work,
//            BindingResult result) {
//        workService.updateWork(work);
//        return "redirect:/uploads.html";
//    }
//    
//    @RequestMapping(value = "/cancel-video.html")
//    public String cancelWork(@Param Integer workId) {
//        Work work = workService.getWorkById(workId);
//        work.setStatus("Canceled");
//        workService.updateWork(work);
//        return "redirect:/uploads.html";
//    }
}
