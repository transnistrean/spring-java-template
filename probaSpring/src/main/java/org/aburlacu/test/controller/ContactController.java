package org.aburlacu.test.controller;

import java.util.ArrayList;
import java.util.Map;

import javassist.expr.NewArray;

import org.aburlacu.test.model.Contact;
import org.aburlacu.test.service.ContactService;
import org.jboss.logging.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


/**
 * Contact Controller 
 * @author Alexandru Burlacu
 */
@Controller
public class ContactController {
 
    @Autowired
    private ContactService contactService;
 
    @Autowired
    private Validator validator;
    
    @RequestMapping(value={"contact.html"})
    public String listContacts(Map<String, Object> map) {
 
        map.put("contact", new Contact());
        map.put("contactList", contactService.listContact());
        
        return "contact";
    }
 
    @RequestMapping(value = "/add-contact.html", method = RequestMethod.POST)
    public String addContact(@ModelAttribute("contact")
    Contact contact, BindingResult result, Map<String, Object> map) {
        String url = "contact";
        validator.validate(contact, result);
         if(!result.hasErrors()) {
            contactService.addContact(contact);
            url = "redirect:/contact.html";
         } else {
            map.put("contactList", contactService.listContact());
         }
        
        return url;
    }
 
    @RequestMapping("/delete-contact.html")
    public String deleteContact(@Param Integer contactId) {
 
        contactService.removeContact(contactId);
 
        return "redirect:/index.html";
    }
}
