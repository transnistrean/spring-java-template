package org.aburlacu.test.controller;

import java.util.ArrayList;
import java.util.Map;

import javassist.expr.NewArray;

import org.aburlacu.test.model.Contact;
import org.aburlacu.test.service.ContactService;
import org.jboss.logging.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


/**
 * Contact Controller 
 * @author Alexandru Burlacu
 */
@Controller
public class SearchController {
 
    @Autowired
    private ContactService contactService;
 
    @Autowired
    private Validator validator;
    
    @RequestMapping("/search.html")
    public String searchContact(@Param String name, Map<String, Object> map) {
        map.put("contactList", contactService.getContactsByName(name));
        return "search";
    }
    
}
