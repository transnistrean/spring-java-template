package org.aburlacu.test.dao;

import java.awt.image.RescaleOp;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.aburlacu.test.model.Contact;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.SharedSessionContract;
import org.hibernate.criterion.Restrictions;
import org.hibernate.hql.internal.ast.tree.RestrictableStatement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.commons.lang3.StringUtils;

/**
 * ContactDAOImpl Class
 * @author Alexandru Burlacu
 */
@Repository
public class ContactDAOImpl implements ContactDAO {
 
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Add contact entry
     * 
     * @param Contact contact - contact object
     * @author Alexandru Burlacu
     */
    public void addContact(Contact contact) {
        sessionFactory.getCurrentSession().save(contact);
    }

    /**
     * Get list of contacts
     * 
     * @return List<Contact> - list of contacts
     * @author Alexandru Burlacu
     */
    public List<Contact> listContact() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Contact where isRemoved=:removed")
                .setBoolean("removed", false);
            return query.list();
    }
 
    /**
     * Remove contact entry by id
     * 
     * @param Integer id - contact id
     * @author Alexandru Burlacu
     */
    public void removeContact(Integer id) {
        Contact contact = (Contact) sessionFactory.getCurrentSession().load(
                Contact.class, id);
        if (null != contact) {
            contact.setIsRemoved(true);
            sessionFactory.getCurrentSession().update(contact);
        }
    }
    
    /**
     * Get contact by id
     * 
     * @param Integer id - contact id
     * @return Contact - contact object
     * @author Alexandru Burlacu
     */
    public Contact getContactById(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        try{
            return (Contact)session.get(Contact.class, id);
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * Get contacts by name
     * 
     * @param String name - name
     * @return List<Contact> - list of contacts
     * @author Alexandru Burlacu
     */
    public List<Contact> getContactsByName(String name) {
        String words[];
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria( Contact.class );
        if (StringUtils.isNotBlank(name)) {
            words = name.split(" ");
            criteria.add(Restrictions.or(
                Restrictions.like("firstname", '%' + name + '%'),
                Restrictions.like("lastname", '%' + name + '%'),
                Restrictions.in("firstname", words),
                Restrictions.in("lastname", words)
                )
            );
        }
        
        criteria.setMaxResults(5);
        List contacts = criteria.list();
        return contacts;
    }
    
}