package org.aburlacu.test.dao;

import java.util.List;

import org.aburlacu.test.model.Contact;

/**
 * Contact DAO interface 
 * @author Alexandru Burlacu
 */
public interface ContactDAO {

    /**
     * Add contact entry
     * 
     * @param Contact contact - contact object
     * @author Alexandru Burlacu
     */
    public void addContact(Contact contact);
    
    /**
     * Get list of contacts
     * 
     * @return List<Contact> - list of contacts
     * @author Alexandru Burlacu
     */
    public List<Contact> listContact();
    
    /**
     * Remove contact entry by id
     * 
     * @param Integer id - contact id
     * @author Alexandru Burlacu
     */
    public void removeContact(Integer id);
    
    /**
     * Get contact by id
     * 
     * @param Integer id - contact id
     * @return Contact - contact object
     * @author Alexandru Burlacu
     */
    public Contact getContactById(Integer id);
    
    /**
     * Get contacts by name
     * 
     * @param String name - name
     * @return List<Contact> - list of contacts
     * @author Alexandru Burlacu
     */
    public List<Contact> getContactsByName(String name);
}
