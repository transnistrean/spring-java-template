package org.aburlacu.test.dao;

import java.util.List;

import org.aburlacu.test.model.Contact;
import org.aburlacu.test.model.Work;

/**
 * Work DAO interface 
 * @author Alexandru Burlacu
 */
public interface WorkDAO {

    /**
     * Add work entry
     * 
     * @param Work work - work object
     * @author Alexandru Burlacu
     */
    public void addWork(Work contact);

    /**
     * Get list of tasks
     * 
     * @return List<Work> - list of tasks
     * @author Alexandru Burlacu
     */
    public List<Work> listWork();
    
    /**
     * Remove work entry by id
     * 
     * @param Integer id - task id
     * @author Alexandru Burlacu
     */
    public void removeWork(Integer id);

    /**
     * Update work entry
     * 
     * @param Work work - work object
     * @author Alexandru Burlacu
     */
    public void updateWork(Work work);
    
    /**
     * Get work by id
     * 
     * @param Integer workId - task id
     * @return Work - work object
     * @author Alexandru Burlacu
     */
    public Work getWorkById(Integer id);
}
