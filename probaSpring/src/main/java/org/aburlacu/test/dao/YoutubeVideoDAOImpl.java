package org.aburlacu.test.dao;

import java.util.List;

import org.aburlacu.test.model.Contact;
import org.aburlacu.test.model.YoutubeVideo;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * YoutubeVideoDAOImpl Class
 * @author Alexandru Burlacu
 */
@Repository
public class YoutubeVideoDAOImpl implements YoutubeVideoDAO {
 
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Add video entry
     * 
     * @param YoutubeVideo video - video object
     * @author Alexandru Burlacu
     */

    public boolean addVideo(YoutubeVideo youtubeVideo) {
        boolean result = false;
        try {
            sessionFactory.getCurrentSession().save(youtubeVideo);
            result = true;
        }
        catch(Exception e)
        {
            //
        }
        
        return result;
    }
 
    /**
     * Get list of tasks
     * 
     * @return List<YoutubeVideo> - list of tasks
     * @author Alexandru Burlacu
     */
    public List<YoutubeVideo> listVideo() {
        Query query = sessionFactory.getCurrentSession().createQuery("from YoutubeVideo where isRemoved=:removed")
            .setBoolean("removed", false);
        return query.list();
    }
 
    /**
     * Remove video entry by id
     * 	
     * @param Integer id - task id
     * @author Alexandru Burlacu
     */
    public void removeVideo(Integer id) {
        YoutubeVideo youtubeVideo = (YoutubeVideo) sessionFactory.getCurrentSession().load(
                YoutubeVideo.class, id);
        youtubeVideo.setIsRemoved(true);
        if (null != youtubeVideo) {
            sessionFactory.getCurrentSession().update(youtubeVideo);
        }
 
    }
    
    /**
     * Update video entry
     * 
     * @param YoutubeVideo video - video object
     * @author Alexandru Burlacu
     */
    public void updateVideo (YoutubeVideo youtubeVideo) {
    	sessionFactory.getCurrentSession().update(youtubeVideo);
    }
    
    /**
     * Get video by id
     * 
     * @param Integer videoId - task id
     * @return YoutubeVideo - video object
     * @author Alexandru Burlacu
     */

    public YoutubeVideo getVideoById(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        try{
            return (YoutubeVideo)session.get(YoutubeVideo.class, id);
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return null;
    }
}