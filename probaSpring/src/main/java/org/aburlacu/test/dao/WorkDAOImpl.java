package org.aburlacu.test.dao;

import java.util.List;

import org.aburlacu.test.model.Contact;
import org.aburlacu.test.model.Work;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * WorkDAOImpl Class
 * @author Alexandru Burlacu
 */
@Repository
public class WorkDAOImpl implements WorkDAO {
 
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Add work entry
     * 
     * @param Work work - work object
     * @author Alexandru Burlacu
     */

    public void addWork(Work work) {
        sessionFactory.getCurrentSession().save(work);
    }
 
    /**
     * Get list of tasks
     * 
     * @return List<Work> - list of tasks
     * @author Alexandru Burlacu
     */
    public List<Work> listWork() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Work where isRemoved=:removed")
            .setBoolean("removed", false);
        return query.list();
    }
 
    /**
     * Remove work entry by id
     * 	
     * @param Integer id - task id
     * @author Alexandru Burlacu
     */
    public void removeWork(Integer id) {
        Work work = (Work) sessionFactory.getCurrentSession().load(
                Work.class, id);
        work.setIsRemoved(true);
        if (null != work) {
            sessionFactory.getCurrentSession().update(work);
        }
 
    }
    
    /**
     * Update work entry
     * 
     * @param Work work - work object
     * @author Alexandru Burlacu
     */
    public void updateWork (Work work) {
    	sessionFactory.getCurrentSession().update(work);
    }
    
    /**
     * Get work by id
     * 
     * @param Integer workId - task id
     * @return Work - work object
     * @author Alexandru Burlacu
     */

    public Work getWorkById(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        try{
            return (Work)session.get(Work.class, id);
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return null;
    }
}