package org.aburlacu.test.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.propertyeditors.ClassEditor;
import org.springframework.util.StringUtils;

public class CustomTimestampEditor extends ClassEditor {

	protected Logger logger = Logger.getLogger(CustomTimestampEditor.class);

	private final SimpleDateFormat format;

	public CustomTimestampEditor(String format) {
		this.format = new SimpleDateFormat(format);
		this.format.setLenient(true);
	}

	public void setAsText(String text) throws IllegalArgumentException {

		if (text.length() == 0) {

			setValue(null);

		} else {

			try {

				logger.debug("Parsing date: " + text);

				Date date = format.parse(text);

				logger.debug("Parsed Date: " + date);

				Timestamp stamp = new Timestamp(date.getTime());

				logger.debug("Parsed Timestamp: " + stamp);

				setValue(stamp);

			} catch (Exception ex) {
				throw new IllegalArgumentException("Value could not be converted: " + text);
			}
		}
	}

	public String getAsText() {

		if (getValue() == null) {
			return null;
		} else {

			return format.format(getValue());
		}
	}
}

