<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

 
<h1 class="contacts">Basic  Search</h1>
 
<form:form method="post" action="search.html" commandName="search">
    <table>
    <tr>
        <td><label for="name"><spring:message code="label.searchCriteria"/></label></td>
        <td>
             <input name="name" type="text">
        </td>
        
    </tr>
</table> 
</form:form>
 
     
<c:if  test="${!empty contactList}">
<h3>Contacts</h3>
<table class="suggestions">
<tr>
    <th>Name</th>
    <th>Email</th>
    <th>Telephone</th>
</tr>
<c:forEach items="${contactList}" var="contact">
    <tr>
        <td>${contact.firstname} ${contact.lastname}</td>
        <td>${contact.email}</td>
        <td>${contact.telephone}</td>
    </tr>
</c:forEach>
</table>
</c:if>

