<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h1 class="contacts">Work Manager</h1>

<form:form method="post" action="add-work.html" commandName="work">
<%--     <form:errors path="*" cssClass="errorblock" element="div" /> --%>
    <table>
        <tr>
            <td>
                <form:label path="contact">
                    <spring:message code="label.userId" />
                </form:label>
            </td>
            <td>
                <select id="contactId" name="contactId">
                    <c:if test="${!empty contactList}">
                        <c:forEach items="${contactList}" var="contact">
                            <option value="${contact.id}">${contact.firstname}
                                ${contact.lastname}</option>
                        </c:forEach>
                    </c:if>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="taskName">
                    <spring:message code="label.taskName" />
                </form:label>
            </td>
            <td>
                <form:input path="taskName" />
                <div>
                    <form:errors path="taskName" cssClass="error" />
                </div>
            </td>
            
        </tr>
        <tr>
            <td>
                <form:label path="description">
                    <spring:message code="label.description" />
                </form:label>
            </td>
            <td>
                <form:input path="description" />
                <div>
                    <form:errors path="description" cssClass="error" />
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="startDate">
                    <spring:message code="label.startDate" />
                </form:label>
            </td>
            <td class="datePickers">
                 <form:input path="startDate"/>
                 <div>
                    <form:errors path="startDate" cssClass="error" />
                 </div>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="endDate">
                    <spring:message code="label.endDate" />
                </form:label>
            </td>
            <td class="datePickers">
                 <form:input path="endDate"/>
                 <div>
                    <form:errors path="endDate" cssClass="error" />
                 </div>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="status">
                    <spring:message code="label.status" />
                </form:label>
            </td>
            <td>
                <form:select path="status" items="${statusList}" />
                <div>
                    <form:errors path="status" cssClass="error" />
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" class="suggestionsButton"
                value="<spring:message code="label.addcontact"/>" />
            </td>
        </tr>
    </table>
</form:form>


<c:if test="${!empty workList}">
    <h3>Tasks</h3>
    <table class="suggestions">
        <tr>
            <th>UserId</th>
            <th>taskName</th>
            <th>description</th>
            <th>startDate</th>
            <th>endDate</th>
            <th>status</th>
            <th>action</th>
        </tr>
        <c:forEach items="${workList}" var="work">
            <tr>
                <td>${work.id}</td>
                <td>${work.taskName}</td>
                <td>${work.description}</td>
                <td>${work.startDate}</td>
                <td>${work.endDate}</td>
                <td>${work.status}</td>
                <td>
                    <a href="delete-work.html?workId=${work.id}">Delete</a>
                    <c:if test="${work.status == 'In progress'}"  >
                        &nbsp;|
                        <a href="work-done.html?workId=${work.id}">Done</a>&nbsp;|
                        <a href="cancel-work.html?workId=${work.id}">Cancel</a>
                    </c:if>
                    
                </td>
            </tr>
        </c:forEach>
    </table>
</c:if>

<link rel="stylesheet"
    href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
$(function() {
    $( ".datePickers input" ).datepicker({
        showOn: "button",
        buttonImage: "resources/images/calendar.png",
        buttonImageOnly: true,
        dateFormat: 'yy-mm-dd'
        });
    });
$( "#datepicker" ).datepicker( "option", "dateFormat", $( this ).val() );
</script>
