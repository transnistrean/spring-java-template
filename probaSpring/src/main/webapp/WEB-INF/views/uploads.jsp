<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h1 class="contacts">Video Manager</h1>

<form:form method="post" id="uploadForm" action="insert-video.html" commandName="video" enctype="multipart/form-data">
 
        
<%--     <form:errors path="*" cssClass="errorblock" element="div" /> --%>
    <table>
       <tr>
            <td>
                <form:label path="videoName">
                    <spring:message code="label.videoName" />
                </form:label>
            </td>
            <td>
                <form:input path="videoName" />
                <div>
                    <form:errors path="videoName" cssClass="error" />
                </div>
            </td>
        </tr>
        
        <tr>
            <td>
                <form:label path="fileData">
                    <spring:message code="label.browseToUpload" />
                </form:label>
            </td>
            <td>
                <form:input path="fileData" type="file"/>
                <div>
                    <form:errors path="fileData" cssClass="error" />
                </div>
            </td>
        </tr>
        
        <tr>
            <td colspan="2">
                <input type="submit" class="suggestionsButton"
                value="<spring:message code="label.uploadvideo"/>" />
            </td>
        </tr>
    </table>
</form:form>


<c:if test="${!empty videoList}">
    <h3>Videos</h3>
    <table class="suggestions">
        <tr>
            <th>Id</th>
            <th>Video name</th>
            <th>Original File Name</th>
            <th>File Path</th>
            <th>Actions</th>
            
        </tr>
        <c:forEach items="${videoList}" var="video">
            <tr>
                <td>${video.id}</td>
                <td>${video.videoName}</td>
                <td>${video.fileName}</td>
                <td>${video.filePath}</td>
                <td>
                    <a href="upload-to-youtube.html?videoId=${video.id}">upload_to_youtube|</a>
                    <a href="delete-video.html?videoId=${video.id}">delete</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</c:if>

<link rel="stylesheet"
    href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
$(function() {
    $( ".datePickers input" ).datepicker({
        showOn: "button",
        buttonImage: "resources/images/calendar.png",
        buttonImageOnly: true,
        dateFormat: 'yy-mm-dd'
        });
    });
$( "#datepicker" ).datepicker( "option", "dateFormat", $( this ).val() );
</script>
