<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

 
<h1 class="contacts">Contact Manager</h1>
 
<form:form method="post" action="add-contact.html" commandName="contact">
    <table>
    <tr>
        <td><form:label path="firstname"><spring:message code="label.firstname"/></form:label></td>
        <td>
            <form:input path="firstname" />
            <div>
                <form:errors path="firstname" cssClass="error" />
            </div>
        </td>
        
    </tr>
    <tr>
        <td><form:label path="lastname"><spring:message code="label.lastname"/></form:label></td>
        <td>
            <form:input path="lastname" />
            <div>
                <form:errors path="lastname" cssClass="error" />
            </div>
        </td>
    </tr>
    <tr>
        <td><form:label path="email"><spring:message code="label.email"/></form:label></td>
        <td>
            <form:input path="email" />
            <div>
                <form:errors path="email" cssClass="error" />
            </div>
        </td>
    </tr>
    <tr>
        <td><form:label path="telephone"><spring:message code="label.telephone"/></form:label></td>
        <td>
            <form:input path="telephone" />
            <div>
                <form:errors path="telephone" cssClass="error" />
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" class="suggestionsButton" value="<spring:message code="label.addcontact"/>"/>
        </td>
    </tr>
</table> 
</form:form>
 
     
<c:if  test="${!empty contactList}">
<h3>Contacts</h3>
<table class="suggestions">
<tr>
    <th>Name</th>
    <th>Email</th>
    <th>Telephone</th>
    <th>&nbsp;</th>
</tr>
<c:forEach items="${contactList}" var="contact">
    <tr>
        <td>${contact.firstname} ${contact.lastname}</td>
        <td>${contact.email}</td>
        <td>${contact.telephone}</td>
        <td><a href="delete-contact.html?contactId=${contact.id}">delete</a></td>
    </tr>
</c:forEach>
</table>
</c:if>

