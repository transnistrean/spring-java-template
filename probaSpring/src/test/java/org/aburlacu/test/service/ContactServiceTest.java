package org.aburlacu.test.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.aburlacu.test.dao.ContactDAO;
import org.aburlacu.test.model.Contact;
import org.aburlacu.test.service.ContactService;
import org.aburlacu.test.service.ContactServiceImpl;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:servlet-context-test.xml" })
public class ContactServiceTest {
	private ContactService contactService;
	private ContactDAO contactDAO;
	private final Integer CONTACT_ID = 2;
	private final String FIRSTNAME = "Jhon";
	private final String LASTNAME = "Watson";
	private final String EMAIL = "test@email.com";
	private final String PHONE = "+112432432432";
	
	@Before
	public void init() {
		contactService = new ContactServiceImpl();
		contactDAO = EasyMock.createMock(ContactDAO.class);
		
		ReflectionTestUtils.setField(contactService, "contactDAO", contactDAO);
	}

	@Test
	public void addContact() {
		Contact contact = new Contact();
		contact.setId(CONTACT_ID);
		contact.setFirstname(FIRSTNAME);
		contact.setLastname(LASTNAME);
		contact.setEmail(EMAIL);
		contact.setTelephone(PHONE);
		
		try {
			contactDAO.addContact(contact);
			EasyMock.replay(contactDAO);
			contactService.addContact(contact);
			EasyMock.verify(contactDAO);
		} catch (Exception exp) {
			assertFalse(true);
		}
	}
	
	@Test
	public void listContact() {
		try {
			EasyMock.expect(contactDAO.listContact())
					.andReturn( new ArrayList<Contact>() );
			EasyMock.replay(contactDAO);
			List<Contact> contacts = contactService.listContact();
			assertNotNull(contacts);
			EasyMock.verify(contactDAO);
		} catch (Exception exp) {
			assertFalse(true);
		} 
	}
	
	@Test
	public void removeContact() {
		try {
			contactDAO.removeContact(CONTACT_ID);
			EasyMock.replay(contactDAO);
			contactService.removeContact(CONTACT_ID);
			EasyMock.verify(contactDAO);
		} catch (Exception exp) {
			assertFalse(true);
		}
	}
	
	@Test
	public void getContactById() {
		try {
			EasyMock.expect(contactDAO.getContactById(CONTACT_ID))
					.andReturn(new Contact());
			EasyMock.replay(contactDAO);
			Contact contact = contactService.getContactById(CONTACT_ID);
			assertNotNull(contact);
			EasyMock.verify(contactDAO);
		} catch (Exception exp) {
			assertFalse(true);
		} 
	}
}
