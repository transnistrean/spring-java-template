package org.aburlacu.test.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.aburlacu.test.dao.WorkDAO;
import org.aburlacu.test.model.Contact;
import org.aburlacu.test.model.Work;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:servlet-context-test.xml" })
public class WorkServiceTest {
	private WorkService workService;
	private WorkDAO workDAO;
	private final Integer CONTACT_ID = 2;
	private final String FIRSTNAME = "Jhon";
	private final String LASTNAME = "Watson";
	private final String EMAIL = "test@email.com";
	private final String PHONE = "+112432432432";
	private final Integer WORK_ID = 2;
	private final String  TASK_NAME = "example of task name";
	private final String  TASK_DESCRIPTION = "example of task description";
	private final Timestamp  START_DATE = new Timestamp(1357423560);
	private final Timestamp  END_DATE = new Timestamp(1357941960);
	private final String STATUS = "In progress";
	
	
	@Before
	public void init() {
		workService = new WorkServiceImpl();
		workDAO = EasyMock.createMock(WorkDAO.class);
		ReflectionTestUtils.setField(workService, "workDAO", workDAO);
	}

	
	@Test
	public void addWork() {
		
		Contact contact = new Contact();
		contact.setId(CONTACT_ID);
		contact.setFirstname(FIRSTNAME);
		contact.setLastname(LASTNAME);
		contact.setEmail(EMAIL);
		contact.setTelephone(PHONE);
		
		Work work = new Work();
		work.setId(WORK_ID);
		work.setContact(contact);
		work.setTaskName(TASK_NAME);
		work.setDescription(TASK_DESCRIPTION);
		work.setStartDate(START_DATE);
		work.setEndDate(END_DATE);
		work.setStatus(STATUS);
		
		try {
			workDAO.addWork(work);
			EasyMock.replay(workDAO);
			workService.addWork(work);
			EasyMock.verify(workDAO);
		} catch (Exception exp) {
			assertFalse(true);
		}
	}
	
	@Test
	public void listWork() {
		try {
			EasyMock.expect(workDAO.listWork())
					.andReturn( new ArrayList<Work>() );
			EasyMock.replay(workDAO);
			List<Work> works = workService.listWork();
			assertNotNull(works);
			EasyMock.verify(workDAO);
		} catch (Exception exp) {
			assertFalse(true);
		} 
	}
	
	@Test
	public void removeWork() {
		try {
			workDAO.removeWork(WORK_ID);
			EasyMock.replay(workDAO);
			workService.removeWork(WORK_ID);
			EasyMock.verify(workDAO);
		} catch (Exception exp) {
			assertFalse(true);
		}
	}
	
	@Test
	public void updateWork() {
		Work work = new Work();
		work.setId(WORK_ID);
		work.setTaskName(TASK_NAME);
		work.setDescription(TASK_DESCRIPTION);
		work.setStartDate(START_DATE);
		work.setEndDate(END_DATE);
		work.setStatus(STATUS);
		try {
			workDAO.updateWork(work);
			EasyMock.replay(workDAO);
			workService.updateWork(work);
			EasyMock.verify(workDAO);
		} catch (Exception exp) {
			assertFalse(true);
		}
	}
	
	@Test
	public void getWorkById() {
		try {
			EasyMock.expect(workDAO.getWorkById(WORK_ID))
					.andReturn(new Work());
			EasyMock.replay(workDAO);
			Work work = workService.getWorkById(WORK_ID);
			assertNotNull(work);
			EasyMock.verify(workDAO);
		} catch (Exception exp) {
			assertFalse(true);
		} 
	}
}
